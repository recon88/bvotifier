package eu.ac3_servers.dev.bvotifier.bungee.net;

import eu.ac3_servers.dev.bvotifier.bungee.BVotifier;
import eu.ac3_servers.dev.bvotifier.bungee.model.Vote;
import eu.ac3_servers.dev.bvotifier.bungee.model.VoteListener;
import eu.ac3_servers.dev.bvotifier.bungee.model.VotifierEvent;
import eu.ac3_servers.dev.bvotifier.bungee.relay.RelayEvent;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.md_5.bungee.api.ProxyServer;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class VoteInboundHandler extends SimpleChannelInboundHandler<Vote>
{
	@Override
	protected void channelRead0( ChannelHandlerContext ctx, final Vote vote ) throws Exception
	{
		// Fire a synchronous task and close the connection.
		BVotifier.getInstance().getProxy().getScheduler().schedule( BVotifier.getInstance(), new Runnable()
		{
			@Override
			public void run()
			{
				for ( VoteListener listener : BVotifier.getInstance().getListeners() ) {
					try {
						listener.voteMade( vote );
					} catch ( Exception ex ) {
						String vlName = listener.getClass().getSimpleName();
						BVotifier.getInstance().getLogger().log( Level.WARNING, "Exception caught while sending the vote notification to the '" + vlName + "' listener", ex );
					}
				}

				VotifierEvent event = new VotifierEvent( vote );
				ProxyServer.getInstance().getPluginManager().callEvent( event );

				if ( !event.isCancelled() ) {
					ProxyServer.getInstance().getPluginManager().callEvent( new RelayEvent( vote ) );
				}
				else if ( BVotifier.getInstance().isDebug() ) {
					BVotifier.getInstance().d( "Vote cancelled by something?" );
				}

			}
		}, 1, TimeUnit.MILLISECONDS );

		ctx.close();

	}

	@Override
	public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception
	{
		VotifierSession session = ctx.channel().attr( VotifierSession.KEY ).get();
		BVotifier.getInstance().getLogger().log( Level.SEVERE, "Exception while processing vote from " + ctx.channel().remoteAddress(), cause );
		ctx.close();
	}
}
