package eu.ac3_servers.dev.bvotifier.bungee;

import com.vexsoftware.votifier.crypto.RSAIO;
import com.vexsoftware.votifier.crypto.RSAKeygen;
import eu.ac3_servers.dev.bvotifier.bungee.commands.BVCommand;
import eu.ac3_servers.dev.bvotifier.bungee.configuration.BVConfig;
import eu.ac3_servers.dev.bvotifier.bungee.model.VoteListener;
import eu.ac3_servers.dev.bvotifier.bungee.net.VoteInboundHandler;
import eu.ac3_servers.dev.bvotifier.bungee.net.VotifierSession;
import eu.ac3_servers.dev.bvotifier.bungee.net.protocol.VotifierGreetingHandler;
import eu.ac3_servers.dev.bvotifier.bungee.net.protocol.VotifierProtocolDifferentiator;
import eu.ac3_servers.dev.bvotifier.bungee6.commands._BVCommand;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

//import java.io.IOException;


/**
 * The main Votifier plugin class.
 *
 * @author Cory Redmond
 * @author Blake Beaupain
 * @author Kramer Campbell
 */
public class BVotifier extends Plugin
{

	public static String channelName = "BVotifier";
	/**
	 * The Votifier instance.
	 */
	private static BVotifier instance;
	/**
	 * The vote listeners.
	 */
	@Getter
	private final List<VoteListener> listeners = new ArrayList<VoteListener>();

	@Getter
	public String version = "1.9";
	public boolean emptysend;
	public boolean singleServerVote;
	public String defaultVoteServer = "lobby";
	public boolean relayEnabled;

	/**
	 * The RSA key pair.
	 */
	@Getter
	private KeyPair keyPair;
	/**
	 * Debug mode flag
	 */
	@Getter
	private boolean debug;
	/**
	 * Configuration
	 */
	@Getter
	private BVConfig cfg;
	/**
	 * Metrics
	 */
	private BungeeMetricsLite metrics;
	/**
	 * Servergroup for netty.
	 */
	private NioEventLoopGroup serverGroup;

	/**
	 * Channel.
	 */
	private Channel serverChannel;

	/**
	 * Gets the instance.
	 *
	 * @return The instance
	 */
	public static BVotifier getInstance()
	{
		return instance;
	}

	@SuppressWarnings({ "static-access" })
	@Override
	public void onEnable()
	{
		BVotifier.instance = this;

		getLogger().info( "Starting metrics." );
		this.metrics = new BungeeMetricsLite( this );
		this.metrics.start();
		getLogger().info( "Started metrics." );

		version = getDescription().getVersion();

		getProxy().registerChannel( channelName );

		try {
			File dataFolder = getDataFolder();
			if ( !dataFolder.exists() ) dataFolder.mkdir();
			File rsaFolder = new File( dataFolder, "rsa" );
			if ( !rsaFolder.exists() ) {
				rsaFolder.mkdir();
				keyPair = RSAKeygen.generate( 2048 );
				RSAIO.save( rsaFolder, keyPair );
			}
			else {
				keyPair = RSAIO.load( rsaFolder );
				getLogger().info( "Loaded RSA files." );
			}
		} catch ( Exception e ) {
			gracefulExit();
			e.printStackTrace();
			return;
		}


		this.cfg = new BVConfig( this );

		String host = cfg.getConfig().getString( "bungee.hostname" );
		int port = cfg.getConfig().getInt( "bungee.port" );
		debug = cfg.getConfig().getBoolean( "both.debug" );
		this.version = cfg.getConfig().getString( "bungee.voteVersion", "1.9" );
		this.emptysend = cfg.getConfig().getBoolean( "bungee.emptysend" );
		this.singleServerVote = cfg.getConfig().getBoolean( "bungee.SingleServerVote.Enabled" );
		this.defaultVoteServer = cfg.getConfig().getString( "bungee.SingleServerVote.RedirectIfNotOnline" );
		this.relayEnabled = cfg.getConfig().getBoolean( "bungee.relayVotes" );

		if ( debug ) {
			getLogger().info( "DEBUG mode enabled!" );
			getLogger().info( "Host: " + host );
			getLogger().info( "Port: " + port );
		}

		serverGroup = new NioEventLoopGroup( 1 );

		new ServerBootstrap()
				.channel( NioServerSocketChannel.class )
				.group( serverGroup )
				.childHandler( new ChannelInitializer<NioSocketChannel>()
				{
					@Override
					protected void initChannel( NioSocketChannel channel ) throws Exception
					{
						channel.attr( VotifierSession.KEY ).set( new VotifierSession() );
						channel.pipeline().addLast( "greetingHandler", new VotifierGreetingHandler() );
						channel.pipeline().addLast( "protocolDifferentiator", new VotifierProtocolDifferentiator() );
						channel.pipeline().addLast( "voteHandler", new VoteInboundHandler() );
					}
				} )
				.bind( host, port )
				.addListener( new ChannelFutureListener()
				{
					@Override
					public void operationComplete( ChannelFuture future ) throws Exception
					{
						if ( future.isSuccess() ) {
							serverChannel = future.channel();
							getLogger().info( "Votifier enabled." );
						}
						else {
							getLogger().log( Level.SEVERE, "Votifier was not able to bind to " + future.channel().localAddress(), future.cause() );
						}
					}
				});

		if ( cfg.getConfig().getBoolean( "StartupOptions.v1_6" ) ) {
			getProxy().getPluginManager().registerCommand( this, new BVCommand( this ) );
		}
		else {
			getProxy().getPluginManager().registerCommand( this, new _BVCommand( this ) );
		}

		if ( !this.relayEnabled ) {
			getLogger().warning( "Vote relay is disabled in config." );
		}
	}

	@Override
	public void onDisable()
	{

		this.metrics.stop();

		if ( serverChannel != null ) serverChannel.close();

	}

	private void gracefulExit()
	{
		getLogger().severe( "Votifier did not initialize properly!" );
		this.metrics.stop();
	}

	/**
	 * Gets the version.
	 *
	 * @return The version
	 */
	public String getVersion()
	{
		return version;
	}


	public void d( Object... text )
	{

		for ( Object object : text ) {

			getLogger().info( ( new StringBuilder( "[DEBUG] " ).append( object ) ).toString() );

		}

	}

}
